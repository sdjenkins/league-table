package com.sdj.pulselive.league;

/**
 * <p>
 * This class represents a match played in the league.
 * </p>
 * <p>
 * There is no date/time reference or validation regarding how many time a team
 * can play another team. So currently, it is assumed that: each team can play
 * each other many times home/away in any given league.
 * </p>
 *
 */
public class Match {

   private int    awayScore;
   private String awayTeam;
   private int    homeScore;
   private String homeTeam;

   public Match(final String homeTeam, final String awayTeam, final int homeScore, final int awayScore) {
      this.homeTeam = homeTeam;
      this.awayTeam = awayTeam;
      this.homeScore = homeScore;
      this.awayScore = awayScore;
   }

   public int getAwayScore() {

      return awayScore;
   }

   public String getAwayTeam() {

      return awayTeam;
   }

   public int getHomeScore() {

      return homeScore;
   }

   public String getHomeTeam() {

      return homeTeam;
   }
}
