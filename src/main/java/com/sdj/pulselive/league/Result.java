package com.sdj.pulselive.league;

public class Result
{

    /**
     * Stores a scratch league entry for the home and away teams from a single
     * given match.
     *
     */

    private LeagueTableEntry awayTeam;
    private LeagueTableEntry homeTeam;

    public Result(LeagueTableEntry homeTeam, LeagueTableEntry awayTeam) {

        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
    }

    public LeagueTableEntry getAwayTeam() {

        return awayTeam;
    }

    public LeagueTableEntry getHomeTeam() {

        return homeTeam;
    }
}
