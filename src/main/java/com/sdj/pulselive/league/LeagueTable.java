package com.sdj.pulselive.league;

import static java.util.stream.Collectors.*;
import static java.util.stream.Collectors.toList;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Create a league table from the supplied list of match results.
 *
 */
public class LeagueTable {


   private static final int DRAW_POINTS  = 1;
   private static final int LOOSE_POINTS = 0;
   private static final int WIN_POINTS = 2;

   private final List<LeagueTableEntry> leagueTable;

   public LeagueTable(final List<Match> matches) {

      Map<String, LeagueTableEntry> tempTable = new HashMap<>();
      accumulateResults(tempTable, matches);
      Comparator<LeagueTableEntry> comparator = new DefaultComparator();
      leagueTable = tempTable.values()
                        .stream()
                        .sorted(comparator::compare)
                        .collect(toList());
   }

   private void accumulateResults(Map<String, LeagueTableEntry> tempTable, List<Match> matches) {

      for (Match match : matches) {
         Result result = computeResult(match);
         updateTable(tempTable, result);
      }
   }

   /**
    * Create a Result object from a given match.
    *
    * @param match
    *           - the given Match object
    *
    * @return Result object
    */
   private Result computeResult(Match match) {

      LeagueTableEntry homeEntry = null;
      LeagueTableEntry awayEntry = null;
      int homePoints;
      int awayPoints;
      int homeDiff = match.getHomeScore() - match.getAwayScore();
      if (homeDiff > 0) {
         homePoints = WIN_POINTS;
         awayPoints = 0;
      } else if (homeDiff == 0) {
         homePoints = DRAW_POINTS;
         awayPoints = DRAW_POINTS;
      } else {
         awayPoints = WIN_POINTS;
         homePoints = 0;
      }
      homeEntry = createEntry(match.getHomeTeam(), homePoints, match.getHomeScore(), match.getAwayScore());
      awayEntry = createEntry(match.getAwayTeam(), awayPoints, match.getAwayScore(), match.getHomeScore());
      return new Result(homeEntry, awayEntry);
   }

   private LeagueTableEntry createEntry(String name, int points, int forGoals, int againstGoals) {

      return new LeagueTableEntry(name, 1, points == WIN_POINTS ? 1 : 0, points == DRAW_POINTS ? 1 : 0,
            points == LOOSE_POINTS ? 1 : 0, forGoals, againstGoals, forGoals - againstGoals, points);
   }

   /**
    * Create an updated entry from two given entries. I.E. accumulate the stats
    * and put in temporary Map
    *
    * @param entries
    *           - the Map
    * @param entry
    *           - 1st entry
    * @param newEntry
    *           - 2nd entry
    */
   private void mergeEntry(Map<String, LeagueTableEntry> entries, LeagueTableEntry entry, LeagueTableEntry newEntry) {

      String teamName = newEntry.getTeamName();
      int played = entry.getPlayed() + newEntry.getPlayed();
      int won = entry.getWon() + newEntry.getWon();
      int drawn = entry.getDrawn() + newEntry.getDrawn();
      int lost = entry.getLost() + newEntry.getLost();
      int goalsFor = entry.getGoalsFor() + newEntry.getGoalsFor();
      int goalsAgainst = entry.getGoalsAgainst() + newEntry.getGoalsAgainst();
      int goalDifference = entry.getGoalDifference() + newEntry.getGoalDifference();
      int points = entry.getPoints() + newEntry.getPoints();
      LeagueTableEntry updatedEntry = new LeagueTableEntry(teamName, played, won, drawn, lost, goalsFor, goalsAgainst,
            goalDifference, points);
      entries.put(teamName, updatedEntry);
   }

   /**
    * Add or update a new team entry in the temporary Map.
    *
    * @param entries
    *           - the Map.
    * @param newEntry
    *           - entry to add or update.
    */
   private void updateEntry(Map<String, LeagueTableEntry> entries, LeagueTableEntry newEntry) {

      String team = newEntry.getTeamName();
      LeagueTableEntry entry = entries.get(team);
      if (entry == null) {
         entries.put(team, newEntry);
      } else {
         mergeEntry(entries, entry, newEntry);
      }
   }

   private void updateTable(Map<String, LeagueTableEntry> tempTable, Result result) {

      updateEntry(tempTable, result.getHomeTeam());
      updateEntry(tempTable, result.getAwayTeam());
   }

   /**
    * Get the ordered list of league table entries for this league table.
    *
    * @return
    */
   public List<LeagueTableEntry> getTableEntries() {

      return leagueTable;
   }

   /**
    * Override toString to display the league table
    *
    * @return A string version of the table in an easy readable form
    */
   public String toString() {

      String table = "         Team          P   W   D   L   F   A  GD  PT\n";
      table += "----------------------------------------------------\n";
      return table + leagueTable.stream().map(LeagueTableEntry::toString).collect(joining("\n"));
   }

}