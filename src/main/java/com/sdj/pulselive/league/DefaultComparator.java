package com.sdj.pulselive.league;

import java.util.Comparator;

public class DefaultComparator implements Comparator<LeagueTableEntry>
{

    /**
     * Create natural sort order for league table entry. Ensure that highest
     * value team will be first when entries are sorted. Strictly - we are making the best team have
     * the lowest rank so that it is shown first in the table. We can give the best team the higher rank
     * however we will have to ensure we sort in reverse order.
     */

    @Override
    public int compare(LeagueTableEntry o1, LeagueTableEntry o2)
    {
        int diff = o2.getPoints() - o1.getPoints();
        if (diff != 0) {
            return diff;
        }

        diff = o2.getGoalDifference() - o1.getGoalDifference();
        if (diff != 0) {
            return diff;
        }

        diff = o2.getGoalsFor() - o1.getGoalsFor();
        if (diff != 0) {
            return diff;
        }
        return o1.getTeamName().compareTo(o2.getTeamName());
    }
}
