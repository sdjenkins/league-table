package com.sdj.pulselive.league;

import static java.lang.String.format;

/**
 * Represents and entry in the team league table with corresponding stats.
 *
 */
public class LeagueTableEntry {

   private int drawn;
   private int goalDifference;
   private int goalsAgainst;
   private int goalsFor;
   private int lost;
   private int played;
   private int points;
   private String teamName;
   private int won;

   public LeagueTableEntry(String teamName, int played, int won, int drawn, int lost, int goalsFor, int goalsAgainst,
         int goalDifference, int points) {
      this.teamName = teamName;
      this.played = played;
      this.won = won;
      this.drawn = drawn;
      this.lost = lost;
      this.goalsFor = goalsFor;
      this.goalsAgainst = goalsAgainst;
      this.goalDifference = goalDifference;
      this.points = points;
   }

   public int getDrawn() {

      return drawn;
   }

   public int getGoalDifference() {

      return goalDifference;
   }

   public int getGoalsAgainst() {

      return goalsAgainst;
   }

   public int getGoalsFor() {

      return goalsFor;
   }

   public int getLost() {

      return lost;
   }

   public int getPlayed() {

      return played;
   }

   public int getPoints() {

      return points;
   }

   public String getTeamName() {

      return teamName;
   }

   public int getWon() {

      return won;
   }


   public String toString() {

      return format("%20s %3d %3d %3d %3d %3d %3d %3d %3d", teamName, played, won, drawn, lost, goalsFor, goalsAgainst, goalDifference, points);
   }
}