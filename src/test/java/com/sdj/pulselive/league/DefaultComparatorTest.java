package com.sdj.pulselive.league;

import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;

public class DefaultComparatorTest
{

   private final static LeagueTableEntry ENTRY1  = new LeagueTableEntry("My Team", 10, 5, 4, 1, 20, 2, 18, 14);
   private final static LeagueTableEntry ENTRY1B = new LeagueTableEntry("My B Team", 10, 5, 4, 1, 19, 2, 17, 14);
   private final static LeagueTableEntry ENTRY1C = new LeagueTableEntry("My C Team", 10, 5, 4, 1, 19, 1, 18, 14);
   private final static LeagueTableEntry ENTRY1D = new LeagueTableEntry("My D Team", 10, 5, 4, 1, 20, 2, 18, 14);
   private final static LeagueTableEntry ENTRY2  = new LeagueTableEntry("Other Team", 10, 4, 4, 2, 20, 2, 18, 10);
   private final static Comparator<LeagueTableEntry> COMPARATOR = new DefaultComparator();

   @Test
   public void testRankOfPointsGoalDiffSameGoalsScoredNotTheSame() {

      Assert.assertTrue(COMPARATOR.compare(ENTRY1, ENTRY1C) < 0);
      Assert.assertTrue(COMPARATOR.compare(ENTRY1C, ENTRY1) > 0);
   }

   @Test
   public void testRankOfPointsNotTheSame() {

      Assert.assertTrue(COMPARATOR.compare(ENTRY2, ENTRY1) > 0);
      Assert.assertTrue(COMPARATOR.compare(ENTRY1, ENTRY2) < 0);
   }

   @Test
   public void testRankOfPointsSameGoalDiffNotTheSame() {

      Assert.assertTrue(COMPARATOR.compare(ENTRY1, ENTRY1B) < 0);
      Assert.assertTrue(COMPARATOR.compare(ENTRY1B, ENTRY1) > 0);
   }

   @Test
   public void testRankWhereAllEqualExceptName() {

      Assert.assertTrue(COMPARATOR.compare(ENTRY1, ENTRY1D) > 0);
      Assert.assertTrue(COMPARATOR.compare(ENTRY1D, ENTRY1) < 0);
   }

}
