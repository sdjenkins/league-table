package com.sdj.pulselive.league;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import static java.util.Arrays.*;

/**
 * Test different match scenarios - not complete
 *
 */
public class LeagueTableTest {

   // TODO Add more result scenarios

   private static final String A = "Team A";
   private static final String B = "Team B";
   private static final String C = "Team C";

   private final static LeagueTableEntry ENTRYA = new LeagueTableEntry(A, 4, 2, 1, 1, 10, 5, 5, 5);
   private final static LeagueTableEntry ENTRYB = new LeagueTableEntry(B, 4, 2, 1, 1, 6, 6, 0, 5);
   private final static LeagueTableEntry ENTRYC = new LeagueTableEntry(C, 4, 1, 0, 3, 3, 8, -5, 2);

   private static final Match[] MATCHES1 =
                  {
                        new Match(A, B, 3, 1),
                        new Match(B, A, 2, 2),
                        new Match(B, C, 1, 0),
                        new Match(C, B, 1, 2),
                        new Match(A, C, 4, 0),
                        new Match(C, A, 2, 1)
                  };

   private void compare(LeagueTable table1, LeagueTableEntry expected, int actual) {

      Assert.assertEquals(expected.getTeamName(), table1.getTableEntries().get(actual).getTeamName());
      Assert.assertEquals(expected.getPlayed(), table1.getTableEntries().get(actual).getPlayed());
      Assert.assertEquals(expected.getWon(), table1.getTableEntries().get(actual).getWon());
      Assert.assertEquals(expected.getDrawn(), table1.getTableEntries().get(actual).getDrawn());
      Assert.assertEquals(expected.getLost(), table1.getTableEntries().get(actual).getLost());
      Assert.assertEquals(expected.getGoalsFor(), table1.getTableEntries().get(actual).getGoalsFor());
      Assert.assertEquals(expected.getGoalsAgainst(), table1.getTableEntries().get(actual).getGoalsAgainst());
      Assert.assertEquals(expected.getGoalDifference(), table1.getTableEntries().get(actual).getGoalDifference());
      Assert.assertEquals(expected.getLost(), table1.getTableEntries().get(actual).getLost());
      Assert.assertEquals(expected.getPoints(), table1.getTableEntries().get(actual).getPoints());
   }

   @Test
   public void testMatch1Senario() {

      LeagueTable table1 = new LeagueTable(asList(MATCHES1));
      compare(table1, ENTRYA, 0);
      compare(table1, ENTRYB, 1);
      compare(table1, ENTRYC, 2);

      System.out.println(table1);
   }

}
